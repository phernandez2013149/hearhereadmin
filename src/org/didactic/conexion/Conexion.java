package org.didactic.conexion;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
public class Conexion {
	public static final Conexion INSTANCIA=new Conexion();
	private SessionFactory session;
	
	public Conexion(){
		try {
			session= new Configuration().configure().buildSessionFactory();
			System.out.println("CONFIGURACION A SIDO FINALIZADA");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public Session getSession() {
		return session.getCurrentSession();
	}


	@SuppressWarnings("unchecked")
	public List<Object> hacerConsulta(String consulta) {
		Session session = this.getSession();
		List <Object> listado=null;
		session.beginTransaction();
		listado=session.createQuery(consulta).list();
		session.getTransaction().commit();
		return listado;
	}
	
	public Object autenticarUsuario(String usuario,String pass){
		Object usr=new Object();
		usr=(Object)hacerConsulta("From Usuario where nick='"+usuario+"' and contrasena='"+pass+"'").get(0);
		return usr;
	}
	
	
	public void modificar(Object obj) {
		Session session = this.getSession();
		session.beginTransaction();
		session.merge(obj);
		session.getTransaction().commit();
	}

	public void agregar(Object obj) {
		Session session = this.getSession();
		session.beginTransaction();
		session.save(obj);
		session.getTransaction().commit();
	}

	public Object Buscar(Class<?> classX, int ID) {
		Session session = this.getSession();
		session.beginTransaction();
		Object objeto=session.get(classX, ID);
		session.getTransaction().commit();
		return objeto;
	}

	public Object Buscar(Class<?> classX, String ID) {
		Session session = this.getSession();
		session.beginTransaction();
		Object objeto=session.get(classX, ID);
		session.getTransaction().commit();
		return objeto;
	}

	public void eliminar(Object obj) {
			Session session = getSession();
			session.beginTransaction();
			session.delete(obj);
			session.getTransaction().commit();
	}
}

