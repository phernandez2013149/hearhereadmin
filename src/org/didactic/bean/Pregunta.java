package org.didactic.bean;

public class Pregunta {
	Integer idPregunta;
	Actividad actividad;
	String nombre;
	Double puntaje;
	String respuesta;
	Boolean esCorrecta;
	public Integer getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}
	public Actividad getActividad() {
		return actividad;
	}
	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getPuntaje() {
		return puntaje;
	}
	public void setPuntaje(Double puntaje) {
		this.puntaje = puntaje;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public Boolean getEsCorrecta() {
		return esCorrecta;
	}
	public void setEsCorrecta(Boolean esCorrecta) {
		this.esCorrecta = esCorrecta;
	}
	public Pregunta(Integer idPregunta, Actividad actividad, String nombre,
			Double puntaje, String respuesta, Boolean esCorrecta) {
		super();
		this.idPregunta = idPregunta;
		this.actividad = actividad;
		this.nombre = nombre;
		this.puntaje = puntaje;
		this.respuesta = respuesta;
		this.esCorrecta = esCorrecta;
	}
	public Pregunta() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
