package org.didactic.bean;

import java.sql.Date;

public class Actividad {
	Integer idActividad;
	TipoActividad tipoActividad;
	Bimestre bimestre;
	String nombre;
	String descripcion;
	Date fechaInicio;
	Date fechaEntrega;
	Double punteoTotal;
	Double punteoObtenido;
	public Integer getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(Integer idActividad) {
		this.idActividad = idActividad;
	}
	public TipoActividad getTipoActividad() {
		return tipoActividad;
	}
	public void setTipoActividad(TipoActividad tipoActividad) {
		this.tipoActividad = tipoActividad;
	}
	public Bimestre getBimestre() {
		return bimestre;
	}
	public void setBimestre(Bimestre bimestre) {
		this.bimestre = bimestre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Double getPunteoTotal() {
		return punteoTotal;
	}
	public void setPunteoTotal(Double punteoTotal) {
		this.punteoTotal = punteoTotal;
	}
	public Double getPunteoObtenido() {
		return punteoObtenido;
	}
	public void setPunteoObtenido(Double punteoObtenido) {
		this.punteoObtenido = punteoObtenido;
	}
	public Actividad(Integer idActividad, TipoActividad tipoActividad,
			Bimestre bimestre, String nombre, String descripcion,
			Date fechaInicio, Date fechaEntrega, Double punteoTotal,
			Double punteoObtenido) {
		super();
		this.idActividad = idActividad;
		this.tipoActividad = tipoActividad;
		this.bimestre = bimestre;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaInicio = fechaInicio;
		this.fechaEntrega = fechaEntrega;
		this.punteoTotal = punteoTotal;
		this.punteoObtenido = punteoObtenido;
	}
	public Actividad() {
		super();
		// TODO Auto-generated constructor stub
	}
}
