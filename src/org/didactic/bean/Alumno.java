package org.didactic.bean;

import java.util.ArrayList;
import java.util.List;

public class Alumno {
	Integer idAlumno;
	Integer carnet;
	String nombre;
	String apellido;
	String username;
	String pass;
	String foto;
    private List<Curso> cursos = new ArrayList<Curso>();
    private List<Seccion> secciones = new ArrayList<Seccion>();

    public void addCurso(Curso curso)
    {
        this.cursos.add(curso);
    }
    public void addSeccion(Seccion seccion)
    {
        this.secciones.add(seccion);
    }
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public List<Seccion> getSecciones() {
		return secciones;
	}
	public void setSecciones(List<Seccion> secciones) {
		this.secciones = secciones;
	}
	public Integer getIdAlumno() {
		return idAlumno;
	}
	public void setIdAlumno(Integer idAlumno) {
		this.idAlumno = idAlumno;
	}
	public Integer getCarnet() {
		return carnet;
	}
	public void setCarnet(Integer carnet) {
		this.carnet = carnet;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Alumno(Integer idAlumno, Integer carnet, String nombre,
			String apellido, String username, String pass, String foto) {
		super();
		this.idAlumno = idAlumno;
		this.carnet = carnet;
		this.nombre = nombre;
		this.apellido = apellido;
		this.username = username;
		this.pass = pass;
		this.foto = foto;
	}
	public Alumno() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
