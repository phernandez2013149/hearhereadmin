package org.didactic.bean;

public class TipoActividad {
	Integer idTipoActividad;
	String nombre;
	public Integer getIdTipoActividad() {
		return idTipoActividad;
	}
	public void setIdTipoActividad(Integer idTipoActividad) {
		this.idTipoActividad = idTipoActividad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public TipoActividad(Integer idTipoActividad, String nombre) {
		super();
		this.idTipoActividad = idTipoActividad;
		this.nombre = nombre;
	}
	public TipoActividad() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
