package org.didactic.bean;

public class Administrador {
	Integer idAdministrador;
	String nombre;
	String apellido;
	String username;
	String pass;
	public Integer getIdAdministrador() {
		return idAdministrador;
	}
	public void setIdAdministrador(Integer idAdministrador) {
		this.idAdministrador = idAdministrador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Administrador(Integer idAdministrador, String nombre,
			String apellido, String username, String pass) {
		super();
		this.idAdministrador = idAdministrador;
		this.nombre = nombre;
		this.apellido = apellido;
		this.username = username;
		this.pass = pass;
	}
	public Administrador() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
