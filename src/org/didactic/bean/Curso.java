package org.didactic.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Curso {
	Integer idCurso;
	String nombre;
	String descripcion;
	Date fechaCreacion;
	Date fechaLimiteAsignacion;
	String pass;
	Administrador administrador;
	Profesor profesor;
	Seccion seccion;
    private List<Alumno> alumnos = new ArrayList<Alumno>();

    public void addAlumno(Alumno alumno)
    {
        this.alumnos.add(alumno);
    }
	public List<Alumno> getAlumnos() {
		return alumnos;
	}
	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	public Integer getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(Integer idCurso) {
		this.idCurso = idCurso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaLimiteAsignacion() {
		return fechaLimiteAsignacion;
	}
	public void setFechaLimiteAsignacion(Date fechaLimiteAsignacion) {
		this.fechaLimiteAsignacion = fechaLimiteAsignacion;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Administrador getAdministrador() {
		return administrador;
	}
	public void setAdministrador(Administrador administrador) {
		this.administrador = administrador;
	}
	public Profesor getProfesor() {
		return profesor;
	}
	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	public Seccion getSeccion() {
		return seccion;
	}
	public void setSeccion(Seccion seccion) {
		this.seccion = seccion;
	}
	public Curso(Integer idCurso, String nombre, String descripcion,
			Date fechaCreacion, Date fechaLimiteAsignacion, String pass,
			Administrador administrador, Profesor profesor, Seccion seccion) {
		super();
		this.idCurso = idCurso;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaCreacion = fechaCreacion;
		this.fechaLimiteAsignacion = fechaLimiteAsignacion;
		this.pass = pass;
		this.administrador = administrador;
		this.profesor = profesor;
		this.seccion = seccion;
	}
	public Curso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
