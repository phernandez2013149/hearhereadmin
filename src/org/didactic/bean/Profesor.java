package org.didactic.bean;

public class Profesor {
	Integer idProfesor;
	Integer codigo;
	String nombre;
	String apellido;
	String username;
	String pass;
	String foto;
	public Integer getIdProfesor() {
		return idProfesor;
	}
	public void setIdProfesor(Integer idProfesor) {
		this.idProfesor = idProfesor;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Profesor(Integer idProfesor, Integer codigo, String nombre,
			String apellido, String username, String pass, String foto) {
		super();
		this.idProfesor = idProfesor;
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.username = username;
		this.pass = pass;
		this.foto = foto;
	}
	public Profesor() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
