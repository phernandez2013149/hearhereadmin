package org.didactic.bean;

import java.util.ArrayList;
import java.util.List;

public class Seccion {
	Integer idSeccion;
	String nombre;
    private List<Alumno> alumnos = new ArrayList<Alumno>();

    public void addAlumno(Alumno alumno)
    {
        this.alumnos.add(alumno);
    }
	public List<Alumno> getAlumnos() {
		return alumnos;
	}
	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	public Integer getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(Integer idSeccion) {
		this.idSeccion = idSeccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Seccion(Integer idSeccion, String nombre) {
		super();
		this.idSeccion = idSeccion;
		this.nombre = nombre;
	}
	public Seccion() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
