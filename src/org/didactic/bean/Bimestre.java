package org.didactic.bean;

public class Bimestre {
	Integer idBimestre;
	Curso curso;
	Integer noBimestre;
	public Integer getIdBimestre() {
		return idBimestre;
	}
	public void setIdBimestre(Integer idBimestre) {
		this.idBimestre = idBimestre;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public Integer getNoBimestre() {
		return noBimestre;
	}
	public void setNoBimestre(Integer noBimestre) {
		this.noBimestre = noBimestre;
	}
	public Bimestre(Integer idBimestre, Curso curso, Integer noBimestre) {
		super();
		this.idBimestre = idBimestre;
		this.curso = curso;
		this.noBimestre = noBimestre;
	}
	public Bimestre() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
