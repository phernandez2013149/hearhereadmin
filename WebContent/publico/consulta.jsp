<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html">

<html>
<head>
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet"
	href="../resources/css/materialize.min.css" media="screen,projection" />
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>

<nav>
	<div class="nav-wrapper">
		<a href="#" class="brand-logo">Logo</a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="../index.jsp">Login</a></li>
			<li><a href="../registro/registroAlumno.jsp">Registrarme</a></li>
		</ul>
	</div>
</nav>

<body>
	<div class="row ">


		<table class="striped centered">
			<thead>
				<tr>
					<th data-field="idBimestre">Bimestre 1</th>
					<th data-field="idBimestre">Bimestre 2</th>
					<th data-field="idBimestre">Bimestre 3</th>
					<th data-field="idBimestre">Bimestre 4</th>
					<th data-field="idBimestre">Bimestre 5</th>
					
					
					
				</tr>
			</thead>

			<tbody>
				<tr>
					
				</tr>
				<tr>
					
				</tr>
				<tr>
					
				</tr>
			</tbody>
		</table>



	</div>
</body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../resources/js/materialize.min.js"></script>
</html>