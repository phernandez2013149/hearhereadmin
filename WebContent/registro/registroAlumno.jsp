<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="../resources/css/materialize.min.css" media="screen,projection" />
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrarse</title>
</head>

<nav>
	<div class="nav-wrapper">
		<a href="#" class="brand-logo">Logo</a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="../index.jsp">Login</a></li>
			<li><a href="registroProfesor.jsp">Soy profesor</a></li>
		</ul>
	</div>
</nav>

<body>
	<div class="col s12 m8 offset-m2 l6 offset-l3">
		<div class="card-panel grey lighten-5 z-depth-1">
			<div class="row valign-wrapper">
				<div class="col s5">
					<form action="">
						<div class="row">
							<div class="input-field col s12">
								<input id="nombre" type="text" class="validate"> <label>Nombre</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="username" type="text" class="validate"> <label
									for="username">Apellido</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="usuario" type="text" class="validate"> <label>Usuario</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="seccion" type="text" class="validate"> <label>Seccion</label>
							</div>
						</div>
						<div class="input-field col s6">
							<input id="password" type="password" class="validate"> <label>Password</label>
						</div>
						<div class="input-field col s6">
							<input id="confirmarPassword" type="password" class="validate">
							<label>Confirmar password</label>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<div class="file-field input-field">
									<input class="file-path validate" type="text" />
									<div class="btn">
										<span>File</span> <input type="file" id="imagen"/>
									</div>
								</div>
							</div>
						</div>

						<button class="btn waves-effect waves-light" type="submit"
							name="action">
							Registrarme <i class="mdi-content-send right"></i>
						</button>

					</form>
				</div>
			</div>
		</div>
	</div>

</body>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../resources/js/materialize.min.js"></script>


</html>