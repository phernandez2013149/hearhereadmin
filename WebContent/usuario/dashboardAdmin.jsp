<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="../resources/css/materialize.min.css" media="screen,projection" />
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrarse</title>
</head>

<nav>
	<div class="nav-wrapper">
		<a href="#" class="brand-logo">Logo</a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="../index.jsp">Login</a></li>
			<li><a href="registroProfesor.jsp">Soy profesor</a></li>
		</ul>
	</div>
</nav>

<ul id="nav-mobile" class="side-nav fixed espacio">
	<li class="logo"></li>
	<li class="bold">
	
	<a href="about.html"	class="waves-effect waves-teal">Sobre Materialize</a></li>
	<li class="bold"><a href="getting-started.html"
		class="waves-effect waves-teal">Introducción</a></li>
	<li class="no-padding">
		<ul class="collapsible collapsible-accordion">
			<li class="bold"><a
				class="collapsible-header  waves-effect waves-teal">CSS</a>
				<div style="" class="collapsible-body">
					<ul>
						<li><a href="color.html">Color</a></li>
						<li><a href="grid.html">Grid</a></li>
						<li><a href="helpers.html">Helpers</a></li>
						<li><a href="media-css.html">Media</a></li>
						<li><a href="sass.html">Sass</a></li>
						<li><a href="shadow.html">Shadow</a></li>
						<li><a href="table.html">Table</a></li>
						<li><a href="typography.html">Typography</a></li>
					</ul>
				</div></li>
			<li class="bold"><a
				class="collapsible-header  waves-effect waves-teal">Components</a>
				<div style="" class="collapsible-body">
					<ul>
						<li><a href="badges.html">Badges</a></li>
						<li><a href="buttons.html">Buttons</a></li>
						<li><a href="cards.html">Cards</a></li>
						<li><a href="collections.html">Collections</a></li>
						<li><a href="footer.html">Footer</a></li>
						<li><a href="forms.html">Forms</a></li>
						<li><a href="icons.html">Icons</a></li>
						<li><a href="navbar.html">Navbar</a></li>
						<li><a href="preloader.html">Preloader</a></li>
					</ul>
				</div></li>
			<li class="bold"><a
				class="collapsible-header  waves-effect waves-teal">JavaScript</a>
				<div style="" class="collapsible-body">
					<ul>
						<li><a href="collapsible.html">Collapsible</a></li>
						<li><a href="dialogs.html">Dialogs</a></li>
						<li><a href="dropdown.html">Dropdown</a></li>
						<li><a href="media.html">Media</a></li>
						<li><a href="modals.html">Modals</a></li>
						<li><a href="parallax.html">Parallax</a></li>
						<li><a href="pushpin.html">Pushpin</a></li>
						<li><a href="scrollfire.html">ScrollFire</a></li>
						<li><a href="scrollspy.html">Scrollspy</a></li>
						<li><a href="side-nav.html">SideNav</a></li>
						<li><a href="tabs.html">Tabs</a></li>
						<li><a href="transitions.html">Transitions</a></li>
						<li><a href="waves.html">Waves</a></li>
					</ul>
				</div></li>
		</ul>
	</li>
	<li class="bold"><a href="http://materializecss.com/mobile.html"
		class="waves-effect waves-teal">Mobile</a></li>
	<li class="bold"><a href="showcase.html"
		class="waves-effect waves-teal">Vitrina</a></li>
</ul>


<body>
	<div class="col s12 m8 offset-m2 l6 offset-l3">
		<div class="card-panel grey lighten-5 z-depth-1">
			<div class="row valign-wrapper">
				<div class="col s5">
					<form action="">
						<div class="row">
							<div class="input-field col s12">
								<input id="nombre" type="text" class="validate"> <label>Nombre</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="username" type="text" class="validate"> <label
									for="username">Apellido</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="usuario" type="text" class="validate"> <label>Usuario</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="seccion" type="text" class="validate"> <label>Seccion</label>
							</div>
						</div>
						<div class="input-field col s6">
							<input id="password" type="password" class="validate"> <label>Password</label>
						</div>
						<div class="input-field col s6">
							<input id="confirmarPassword" type="password" class="validate">
							<label>Confirmar password</label>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<div class="file-field input-field">
									<input class="file-path validate" type="text" />
									<div class="btn">
										<span>File</span> <input type="file" id="imagen" />
									</div>
								</div>
							</div>
						</div>

						<button class="btn waves-effect waves-light" type="submit"
							name="action">
							Registrarme <i class="mdi-content-send right"></i>
						</button>

					</form>
				</div>
			</div>
		</div>
	</div>

</body>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../resources/js/materialize.min.js"></script>


</html>