<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html">

 
<html>
    <head>
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="resources/css/materialize.min.css"  media="screen,projection"/>
      <meta charset= "utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>
    
	<body>
	
	<nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Logo</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="publico/consulta.jsp">Notas</a></li>
        <li><a href="registro/registroAlumno.jsp">Registrarme</a></li>
      </ul>
    </div>
  </nav>
	
	<div class="container col s12 m2">
       <div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Login</span>
					<div class="row">
					  <form class="col s12" action="usuario/dashboardAdmin.jsp">
					    <div class="row">
					    </div>
					    <div class="row">
					      <div class="input-field col s12">
					        <input id="username" type="text" class="validate">
					        <label for="username">Username</label>
					      </div>
					    </div>
					    <div class="row">
					      <div class="input-field col s12">
					        <input id="password" type="password" class="validate">
					        <label for="password">Password</label>
					      </div>
					    </div>
					    <button class="btn waves-effect waves-light" type="submit" name="action">Login
					    	<i class="mdi-content-send right"></i>
						</button>
					  </form>
					</div>
            </div>
          </div>
        </div>
      </div>
	</div>
      
      </body>
     <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="resources/js/materialize.min.js"></script>
</html>